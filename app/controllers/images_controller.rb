# frozen_string_literal: true

class ImagesController < ApplicationController
  def create
    result = Images::ConvertService.new(image_params).execute

    case result[:status]
    when :success
      send_file result[:image_path], type: 'image/png', disposition: 'inline'
    when :error
      render text: result[:message], status: 422
    end
  end

  private

  def image_params
    params.permit(:file, commands: [])
  end
end
