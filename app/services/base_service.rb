# frozen_string_literal: true

class BaseService
  attr_accessor :params

  def initialize(params = {})
    @params = params.dup
  end

  protected

  def error(message, http_status = nil)
    result = {
      message: message,
      status: :error
    }

    result[:http_status] = http_status if http_status
    result
  end

  def success(pass_back = {})
    pass_back[:status] = :success
    pass_back
  end
end
