# frozen_string_literal: true

require 'fileutils'
require 'mini_magick'

module Images
  class ConvertService < BaseService
    def execute
      FileUtils.mkdir_p(storage_path)
      image_path = "#{storage_path}/#{SecureRandom.uuid}.png"

      MiniMagick::Tool::Convert.new do |convert|
        convert << convert_params[:file].path
        convert.merge!(convert_params[:commands]) if convert_params[:commands]
        convert.format('png')
        convert << image_path
      end

      success(image_path: image_path)
    rescue StandardError => e
      error(e.message)
    end

    private

    def storage_path
      "#{Rails.root}/public/images/#{Time.new.strftime('%Y/%m/%d')}"
    end

    def convert_params
      params.slice(:file, :commands)
    end
  end
end
