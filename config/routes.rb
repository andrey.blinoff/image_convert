# frozen_string_literal: true

Rails.application.routes.draw do
  resource :images, only: :create
end
