# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ImagesController, type: :controller do
  let(:file) do
    f = File.open(File.join(Rails.root, '/spec/fixtures/test_image.png'))
    Rack::Test::UploadedFile.new(f, 'image/png')
  end

  context 'POST /images' do
    it 'convert image' do
      post :create, params: { file: file, commands: %w[-resize 100x100] }
      expect(response).to have_http_status(:ok)
    end
  end
end
