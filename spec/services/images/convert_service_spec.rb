# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Images::ConvertService do
  describe '#execute' do
    let(:file) { File.open(File.join(Rails.root, '/spec/fixtures/test_image.png')) }
    let(:result) { described_class.new(opts).execute }

    context 'when file and commands are valid' do
      let(:opts) do
        {
          file: file,
          commands: %w[-resize 100x100]
        }
      end

      it 'convert image' do
        expect(result[:status]).to eq :success
        expect(result[:image_path]).to be_present
      end
    end

    context 'when file corrupted' do
      let(:file) { File.open(File.join(Rails.root, '/spec/fixtures/test_image.txt')) }

      let(:opts) do
        {
          file: file,
          commands: %w[-resize 100x100]
        }
      end

      it 'convert image' do
        expect(result[:status]).to eq :error
      end
    end

    context 'imagemagick commands corrupted' do
      let(:opts) do
        {
          file: file,
          commands: %w[-resize 100x100 -notacommand]
        }
      end

      it 'convert image' do
        expect(result[:status]).to eq :error
      end
    end
  end
end
